package crook

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	cfg := Config{}
	c, err := New(&cfg)
	assert.Nil(t, err)
	assert.Equal(t, &cfg, c.cfg)
}
