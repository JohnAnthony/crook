package crook

import (
	"fmt"
	"io/fs"
	"syscall"
	"testing"

	"github.com/stretchr/testify/assert"
	yaml "gopkg.in/yaml.v2"
)

func TestDefaultConfig(t *testing.T) {
	expect := Config{
		Tasks: make(map[GitHook][]Task),
	}

	assert.Equal(t, expect, DefaultConfig())
}

func TestLoadConfigYAML(t *testing.T) {
	testCases := []struct {
		description string
		path        string
		expectRet   Config
		expectErr   error
	}{
		{
			description: "invalid path",
			path:        "dksflskl",
			expectRet:   Config{},
			expectErr: fmt.Errorf(
				"Error opening config file dksflskl: %w",
				&fs.PathError{
					Op:   "open",
					Path: "dksflskl",
					Err:  syscall.Errno(2),
				},
			),
		},
		{
			description: "invalid yaml",
			path:        "testdata/config_invalid.yaml",
			expectRet:   Config{},
			expectErr: fmt.Errorf(
				"Error opening config file testdata/config_invalid.yaml: %w",
				&yaml.TypeError{
					Errors: []string{
						"line 1: cannot unmarshal !!str `this is...` into crook.configFile",
					},
				},
			),
		},
		{
			description: "valid but empty config file",
			path:        "testdata/config_valid_but_empty.yaml",
			expectRet: Config{
				Tasks: make(map[GitHook][]Task),
			},
			expectErr: nil,
		},
	}

	t.Parallel()
	for _, tc := range testCases {
		tc := tc // capture
		t.Run(tc.description, func(t *testing.T) {
			t.Parallel()

			ret, err := LoadConfigYAML(tc.path)
			assert.Equal(t, tc.expectRet, ret)
			assert.Equal(t, tc.expectErr, err)
		})
	}
}
