package main

import (
	_ "embed"
	"errors"
	"flag"
	"fmt"
	"os"

	"gitlab.com/JohnAnthony/crook"
)

var (
	//go:embed usage-global.txt
	usageGlobalStr string
	//go:embed default-config.yaml
	defaultConfig string
)

type subCommand struct {
	fn         func(configPath string, args []string)
	requireGit bool
}

func mainExec(configPath string, args []string) {
	if len(args) < 1 {
		fmt.Println("exec requires a hook name: `crook exec <hook> <files...>`")
		os.Exit(1)
	}

	files := args[1:]
	hook := crook.GitHook(args[0])
	if !hook.IsValid() {
		fmt.Printf("invalid hook name: %s\n", hook)
		os.Exit(1)
	}

	cfg, err := crook.LoadConfigYAML(configPath)
	if errors.Is(err, os.ErrNotExist) {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(
			os.Stderr,
			"If you are running crook for the first time, try `crook init`",
		)
		os.Exit(1)
	}
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	c, err := crook.New(&cfg)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if err := c.Exec(hook, files); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func mainHelp(configPath string, args []string) {
	// TODO: Args

	usageGlobal()
}

func mainInit(configPath string, args []string) {
	// TODO: Args

	_, err := os.Stat(configPath)
	switch {
	case errors.Is(err, os.ErrNotExist):
		// This is our happy path
	case err != nil:
		fmt.Fprintf(
			os.Stderr,
			"Error finding config file %s: %s",
			configPath,
			err,
		)
		os.Exit(1)
	default:
		fmt.Printf("~ File already exists at %s\n", configPath)
		fmt.Println("")
		fmt.Println("run `crook sync` if your hooks need updating")
		fmt.Println("run `git config core.hooksPath` to ensure git is configured for crook")
		fmt.Println("run `git config core.hooksPath .crook` to configure git to use the default hooksPath")
		fmt.Println("")
		os.Exit(0)
	}

	file, err := os.Create(configPath)
	if err != nil {
		fmt.Fprintf(
			os.Stderr,
			"Error creating config file %s: %s",
			configPath,
			err,
		)
		os.Exit(1)
	}

	if _, err := file.WriteString(defaultConfig); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	fmt.Printf("+ Created %s\n", configPath)

	cfg, err := crook.LoadConfigYAML(configPath)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	c, err := crook.New(&cfg)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if err := c.Sync(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	fmt.Println("")
	fmt.Printf(
		"run `git config core.hooksPath %s` to use crook hooks\n",
		crook.HooksPath,
	)
	fmt.Println("")
}

func mainNone(string, []string) {
	fmt.Fprintln(os.Stderr, "Unable to parse arguments")
	usageGlobal()
	os.Exit(1)
}

func mainSync(configPath string, args []string) {
	// TODO: Args

	cfg, err := crook.LoadConfigYAML(configPath)
	if errors.Is(err, os.ErrNotExist) {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(
			os.Stderr,
			"If you are running crook for the first time, try `crook init`",
		)
		os.Exit(1)
	}
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	c, err := crook.New(&cfg)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if err := c.Sync(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func usageGlobal() {
	fmt.Println(usageGlobalStr)
}

func main() {
	// Find which sub-command is being requested
	mainCommands := map[string]subCommand{
		"":     {mainNone, false},
		"exec": {mainExec, true},
		"help": {mainHelp, false},
		"init": {mainInit, true},
		"sync": {mainSync, true},
	}

	globalArgs := os.Args[1:]
	subCmd := mainCommands[""]
	var subArgs []string
	for i, arg := range os.Args[1:] {
		if sc, found := mainCommands[arg]; found {
			globalArgs = os.Args[1 : i+1]
			subCmd = sc
			subArgs = os.Args[i+2:]
			break
		}
	}

	// Parse global flags (before sub-command)
	var configPath = ".crook.yaml"
	globalFlagSet := flag.NewFlagSet("global", flag.ContinueOnError)
	globalFlagSet.Usage = usageGlobal
	globalFlagSet.StringVar(&configPath, "c", ".crook.yaml", "")
	globalFlagSet.StringVar(&configPath, "config", ".crook.yaml", "")
	if err := globalFlagSet.Parse(globalArgs); err != nil {
		fmt.Fprintf(os.Stderr, "Unable to parse global arguments: %s", err)
		os.Exit(1)
	}

	if subCmd.requireGit {
		// Ensure we're in a git diretory and at the root

		gitRoot, err := crook.GitRoot()
		if err != nil {
			fmt.Printf("Error finding git root: %s\n", err)
			fmt.Println("Are you sure you're in a git repository?")
			os.Exit(1)
		}
		if err := os.Chdir(gitRoot); err != nil {
			fmt.Fprintf(os.Stderr, "Error doing chdir to git root: %s", err)
			os.Exit(1)
		}
	}

	// Run sub-command
	subCmd.fn(configPath, subArgs)
}
