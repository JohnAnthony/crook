# Crook

A lightweight, single binary git hook management tool.

![Logo](logo.png)

## Installation

### Build the binary

```bash
go install gitlab.com/JohnAnthony/crook/cmd/crook@latest
```

### Add to your project

```bash
crook init
```

Once this command is run, `.crook.yaml` and `.crook/` will be created. These are
your config file and hooks directory. You will also be informed of any further
setup required by the output of this command - usually that you need to run
`git config core.hooksPath .crook` to switch to using Crook hooks.

### Getting help

Simply run `crook` or `crook help` for a summary of Crook's sub-commands.

### How it works

Git has a config option of `core.hooksPath`, which defaults to `.git/hooks` if
not set. Crook creates and manages a folder at `.crook` with hooks generated
from tasks defined in `.crook.yaml`. These hooks also run Crook with the `exec`
sub-command to run tasks for each hook.

Crook makes hooks (`init`/`sync`) and those hooks run Crook (`exec`).

## Config

The config file is in YAML, and allows you to define tasks to run for different
hooks. By default, only the pre-commit hook is installed - see the next section
for important information on how to sync installed hooks with those defined in
the config file.

```yaml
tasks:
- name: example task                                # Required
  exec: echo this is not a real command && return 1 # Required
  hook: pre-commit                                  # Optional
  glob: '*'                                         # Optional
  dir: '.'                                          # Optional
  replace: '{}'                                     # Optional
```

`name` is shown when the task is run \
`exec` is the command to run when the hook is triggered \
`hook` is the git hook that should trigger the command; see the section on
  syncing below for adding or removing hooks to match the config file \
`glob` has different behaviour depending upon the hook; see the Hooks section
  below \
`dir` is the directory in which to run the command \
`replace` is the pattern to replace in the `exec` command, and injects different
  values depending upon the hook; see the Hooks section below

### Sync / using hooks other than pre-commit

By default, only the pre-commit hook is installed in `.crook/`. If `.crook.yaml`
is changed to include new hooks then running the following command will cause
Crook to match up installed hooks with those used in `crook.yaml`

```bash
crook sync
```

### Examples

Run golangci-lint on commits that contain .go files, fixing any issues that can
be automatically fixed

```yaml
tasks:
- name: lint
  exec: golangci-lint run --fix
  glob: '*.go'
```

Run eslint and prettier on all changed files of a suitable type, fixing any
issues that can be automatically fixed

```yaml
tasks:
- name: eslint
  exec: ./node_modules/.bin/eslint --fix {}
  glob: '*.{ts,tsx}'
- name: prettier
  exec: ./node_modules/.bin/prettier --write {}
  glob: '*.{ts,tsx,js,jsx,md,json}'
```

## Hooks

### pre-commit

In this hook's script, {} will be replaced with a list of files that have been
modified.

Using glob in a pre-commit task will cause the list of files to be filtered to
only those which match the glob. If all are filtered, exec is not run.

### commit-msg

In this task's exec, {} will be replaced with the path to the commit message
file.

Using glob in a commit-msg task will cause exec to only be run if the commit
message matches the glob provided.
