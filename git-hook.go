package crook

type GitHook string

const (
	GitHookApplyPatchMsg    GitHook = "applypatch-msg"
	GitHookCommitMsg        GitHook = "commit-msg"
	GitHookPostApplyPatch   GitHook = "post-applypatch"
	GitHookPostCheckout     GitHook = "post-checkout"
	GitHookPostCommit       GitHook = "post-commit"
	GitHookPostMerge        GitHook = "post-merge"
	GitHookPostReceive      GitHook = "post-receive"
	GitHookPostRewrite      GitHook = "post-rewrite"
	GitHookPostUpdate       GitHook = "post-update"
	GitHookPreApplyPatch    GitHook = "pre-applypatch"
	GitHookPreAutoGC        GitHook = "pre-auto-gc"
	GitHookPreCommit        GitHook = "pre-commit"
	GitHookPrePush          GitHook = "pre-push"
	GitHookPreRebase        GitHook = "pre-rebase"
	GitHookPreReceive       GitHook = "pre-receive"
	GitHookPrepareCommitMsg GitHook = "prepare-commit-msg"
	GitHookUpdate           GitHook = "update"
)

var validHooks = map[GitHook]struct{}{
	GitHookApplyPatchMsg:    struct{}{},
	GitHookCommitMsg:        struct{}{},
	GitHookPostApplyPatch:   struct{}{},
	GitHookPostCheckout:     struct{}{},
	GitHookPostCommit:       struct{}{},
	GitHookPostMerge:        struct{}{},
	GitHookPostReceive:      struct{}{},
	GitHookPostRewrite:      struct{}{},
	GitHookPostUpdate:       struct{}{},
	GitHookPreApplyPatch:    struct{}{},
	GitHookPreAutoGC:        struct{}{},
	GitHookPreCommit:        struct{}{},
	GitHookPrePush:          struct{}{},
	GitHookPreRebase:        struct{}{},
	GitHookPreReceive:       struct{}{},
	GitHookPrepareCommitMsg: struct{}{},
	GitHookUpdate:           struct{}{},
}

func (gs GitHook) IsValid() bool {
	_, ok := validHooks[gs]
	return ok
}
