package crook

import (
	"fmt"
	"strings"

	"github.com/gobwas/glob"
)

type configFile struct {
	Tasks []configTask
}

type configTask struct {
	Dir     *string
	Exec    string
	Glob    *string
	Hook    *GitHook
	Name    string
	Replace *string
}

func (cf *configFile) ToConfig() (Config, error) {
	c := DefaultConfig()

	for _, ct := range cf.Tasks {
		task, err := ct.ToTask()
		if err != nil {
			return Config{}, err
		}

		if _, found := c.Tasks[task.Hook]; !found {
			c.Tasks[task.Hook] = make([]Task, 0, 1)
		}
		c.Tasks[task.Hook] = append(c.Tasks[task.Hook], task)
	}

	return c, nil
}

func (ct *configTask) ToTask() (Task, error) {
	name := strings.TrimSpace(ct.Name)
	if name == "" {
		return Task{}, fmt.Errorf("%w: %v", ErrInvalidTaskName, ct)
	}

	exec := strings.TrimSpace(ct.Exec)
	if exec == "" {
		return Task{}, fmt.Errorf("%w: %v", ErrInvalidTaskExec, ct)
	}

	var dir string
	if ct.Dir != nil {
		dir = *ct.Dir
	}

	var err error
	var globMatch glob.Glob
	if ct.Glob != nil {
		globMatch, err = glob.Compile(*ct.Glob)
		if err != nil {
			return Task{}, fmt.Errorf(
				"Error compiling glob (%s): %w",
				*ct.Glob,
				err,
			)
		}
	}

	var hook = GitHookPreCommit
	if ct.Hook != nil {
		hook = *ct.Hook
	}
	if !hook.IsValid() {
		return Task{}, fmt.Errorf("%w: %v", ErrInvalidTaskHook, ct)
	}

	var replace = "{}"
	if ct.Replace != nil {
		replace = strings.TrimSpace(*ct.Replace)
	}
	if replace == "" {
		return Task{}, fmt.Errorf("%w: %v", ErrInvalidTaskReplace, ct)
	}

	return Task{
		Name:    name,
		Dir:     dir,
		Exec:    exec,
		Glob:    globMatch,
		Hook:    hook,
		Replace: replace,
	}, nil
}

func (ct configTask) String() string {
	hookName := "<nil>"
	if ct.Hook != nil {
		hookName = string(*ct.Hook)
	}

	return fmt.Sprintf(
		`name="%s" exec="%s" hook=%v glob=%v dir=%v`,
		ct.Name,
		ct.Exec,
		hookName,
		ct.Glob,
		ct.Dir,
	)
}
