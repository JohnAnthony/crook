package crook

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEscapeFilename(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		in  string
		out string
	}{
		{
			in:  `foo.go`,
			out: `foo.go`,
		},
		{
			in:  `foo/bar/baz.go`,
			out: `foo/bar/baz.go`,
		},
		{
			in:  `name with spaces.go`,
			out: `name\ with\ spaces.go`,
		},
	}

	for _, tc := range testCases {
		tc := tc // Capture inside loop

		t.Run(tc.in, func(t *testing.T) {
			t.Parallel()

			assert.Equal(t, tc.out, escapeFilename(tc.in))
		})
	}
}
