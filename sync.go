package crook

import (
	"embed"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
)

//go:embed hookfiles
var hookfiles embed.FS

func (c *Crook) Sync() error {
	// Remove old folder if it exists
	err := os.RemoveAll(HooksPath)
	switch {
	case errors.Is(err, os.ErrNotExist):
		// No problem
	case err != nil:
		return fmt.Errorf("Error removing old HooksPath: %w", err)
	}

	// Make folder, replacing the old one if there was one
	err = os.Mkdir(HooksPath, 0755)
	switch {
	case errors.Is(err, os.ErrExist):
		// No problem
	case err != nil:
		return fmt.Errorf("Error creating HooksPath (%s): %w", HooksPath, err)
	}

	// All the hooks we need to create
	configHooks := make([]string, 0, len(c.cfg.Tasks))
	for hook := range c.cfg.Tasks {
		configHooks = append(configHooks, string(hook))
	}
	sort.Strings(configHooks)

	// Insert all the hooks from the latest versions
	for _, hook := range configHooks {
		inPath := filepath.Join("hookfiles/", hook)
		outPath := filepath.Join(HooksPath, hook)

		hookfileBytes, err := hookfiles.ReadFile(inPath)
		if err != nil {
			return err
		}

		if err := ioutil.WriteFile(outPath, hookfileBytes, 0755); err != nil {
			return err
		}
	}

	return nil
}
