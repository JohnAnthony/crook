package crook

import (
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"

	"github.com/gobwas/glob"
)

var escapeFilenameRegex = regexp.MustCompile("[^A-Za-z0-9/.]")

func (c *Crook) Exec(hook GitHook, allFiles []string) error {
	tasks, found := c.cfg.Tasks[hook]
	if !found {
		// No tasks to do for this hook in our config
		return nil
	}

	for _, task := range tasks {
		files := allFiles
		if task.Glob != nil {
			files = filterFiles(task.Glob, files)
			if len(files) == 0 {
				continue
			}
		}
		cmd := createCmd(&task, files)

		fmt.Printf("<crook task: %s>\n", task.Name)
		if err := cmd.Run(); err != nil {
			return fmt.Errorf("Error running cmd (%s): %w", task.Exec, err)
		}
	}

	return nil
}

func filterFiles(g glob.Glob, files []string) []string {
	ret := make([]string, 0)
	for _, file := range files {
		if g.Match(file) {
			ret = append(ret, file)
		}
	}
	return ret
}

func createCmd(t *Task, files []string) *exec.Cmd {
	escapedFiles := make([]string, 0, len(files))
	for _, file := range files {
		escapedFiles = append(escapedFiles, escapeFilename(file))
	}

	cmd := exec.Command(
		"bash",
		"-c",
		strings.ReplaceAll(t.Exec, t.Replace, strings.Join(escapedFiles, " ")),
	)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if t.Dir != "" {
		cmd.Dir = t.Dir
	}

	return cmd
}

func escapeFilename(file string) string {
	return escapeFilenameRegex.ReplaceAllString(file, `\$0`)
}
