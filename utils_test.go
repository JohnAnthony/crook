package crook

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGitRootSuccess(t *testing.T) {
	dir, err := os.Getwd()
	require.Nil(t, err)

	root, err := GitRoot()
	require.Nil(t, err)
	assert.Equal(t, dir, root)
}

func TestGitRootFailure(t *testing.T) {
	wd, err := os.Getwd()
	require.Nil(t, err)

	// Note: Technically this causes a race condition because of the tests all
	// using a single working directory, but I haven't observed this actually
	// causing the test to be flaky
	err = os.Chdir(t.TempDir())
	defer os.Chdir(wd)

	require.Nil(t, err)
	_, err = GitRoot()
	assert.NotNil(t, err)
}
