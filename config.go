package crook

import (
	"errors"
	"fmt"
	"os"

	"github.com/gobwas/glob"
	yaml "gopkg.in/yaml.v2"
)

var (
	ErrInvalidTaskName    = errors.New("Invalid task name")
	ErrInvalidTaskExec    = errors.New("Invalid task exec")
	ErrInvalidTaskHook    = errors.New("Invalid task hook")
	ErrInvalidTaskReplace = errors.New("Invalid task replace")
)

type Config struct {
	Tasks map[GitHook][]Task
}

type Task struct {
	Name    string
	Dir     string
	Exec    string
	Glob    glob.Glob
	Hook    GitHook
	Replace string
}

func DefaultConfig() Config {
	return Config{
		Tasks: make(map[GitHook][]Task),
	}
}

func LoadConfigYAML(path string) (Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return Config{}, fmt.Errorf(
			"Error opening config file %s: %w",
			path,
			err,
		)
	}

	var conf configFile
	if err := yaml.NewDecoder(file).Decode(&conf); err != nil {
		return Config{}, fmt.Errorf(
			"Error opening config file %s: %w",
			path,
			err,
		)
	}

	return conf.ToConfig()
}
