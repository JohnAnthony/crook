package crook

import (
	"errors"
	"fmt"
	"testing"

	"github.com/gobwas/glob"
	"github.com/stretchr/testify/assert"
)

func TestConfigFileToConfig(t *testing.T) {
	testCases := []struct {
		description string
		configFile  configFile
		expectRet   Config
		expectErr   error
	}{
		{
			description: "simple empty",
			configFile: configFile{
				Tasks: []configTask{},
			},
			expectRet: Config{
				Tasks: map[GitHook][]Task{},
			},
			expectErr: nil,
		},
		{
			description: "a task",
			configFile: configFile{
				Tasks: []configTask{
					{
						Name: "some task",
						Exec: "foo bar baz",
					},
				},
			},
			expectRet: Config{
				Tasks: map[GitHook][]Task{
					GitHookPreCommit: {
						{
							Name:    "some task",
							Dir:     "",
							Exec:    "foo bar baz",
							Hook:    "pre-commit",
							Replace: "{}",
						},
					},
				},
			},
			expectErr: nil,
		},
		{
			description: "task with an error",
			configFile: configFile{
				Tasks: []configTask{
					{
						Name: "", // invalid
						Exec: "foo bar baz",
					},
				},
			},
			expectRet: Config{},
			expectErr: fmt.Errorf(
				"%w: %v",
				ErrInvalidTaskName,
				configTask{
					Name: "",
					Exec: "foo bar baz",
				},
			),
		},
	}

	t.Parallel()
	for _, tc := range testCases {
		tc := tc // capture
		t.Run(tc.description, func(t *testing.T) {
			t.Parallel()

			ret, err := tc.configFile.ToConfig()
			assert.Equal(t, tc.expectRet, ret)
			assert.Equal(t, tc.expectErr, err)
		})
	}
}

func TestConfigTaskToTask(t *testing.T) {
	justHook := func(h GitHook) *GitHook { return &h }
	justStr := func(s string) *string { return &s }

	testCases := []struct {
		description string
		configTask  configTask
		expectRet   Task
		expectErr   error
	}{
		{
			description: "empty exec",
			configTask: configTask{
				Name: "some task",
				Exec: "",
			},
			expectRet: Task{},
			expectErr: fmt.Errorf(
				"%w: %v",
				ErrInvalidTaskExec,
				configTask{
					Name: "some task",
					Exec: "",
				},
			),
		},
		{
			description: "exec is all whitespace",
			configTask: configTask{
				Name: "some task",
				Exec: "  	 	  	 ",
			},
			expectRet: Task{},
			expectErr: fmt.Errorf(
				"%w: %v",
				ErrInvalidTaskExec,
				configTask{
					Name: "some task",
					Exec: "  	 	  	 ",
				},
			),
		},
		{
			description: "minimal",
			configTask: configTask{
				Name: "some task",
				Exec: "foo bar baz",
			},
			expectRet: Task{
				Name:    "some task",
				Exec:    "foo bar baz",
				Hook:    GitHookPreCommit,
				Replace: "{}",
			},
			expectErr: nil,
		},
		{
			description: "different hook",
			configTask: configTask{
				Name: "some task",
				Exec: "foo bar baz",
				Hook: justHook(GitHookPostCommit),
			},
			expectRet: Task{
				Name:    "some task",
				Exec:    "foo bar baz",
				Hook:    GitHookPostCommit,
				Replace: "{}",
			},
			expectErr: nil,
		},
		{
			description: "invalid hook",
			configTask: configTask{
				Name: "some task",
				Exec: "foo bar baz",
				Hook: justHook(GitHook("invalid-hook-here")),
			},
			expectRet: Task{},
			expectErr: fmt.Errorf(
				"%w: %v",
				ErrInvalidTaskHook,
				configTask{
					Name: "some task",
					Exec: "foo bar baz",
					Hook: justHook(GitHook("invalid-hook-here")),
				},
			),
		},
		{
			description: "set dir",
			configTask: configTask{
				Name: "some task",
				Exec: "foo bar baz",
				Dir:  justStr("/some/other/dir"),
			},
			expectRet: Task{
				Name:    "some task",
				Exec:    "foo bar baz",
				Hook:    GitHookPreCommit,
				Dir:     "/some/other/dir",
				Replace: "{}",
			},
			expectErr: nil,
		},
		{
			description: "set glob",
			configTask: configTask{
				Name: "some task",
				Exec: "foo bar baz",
				Glob: justStr("*.go"),
			},
			expectRet: Task{
				Name:    "some task",
				Exec:    "foo bar baz",
				Hook:    GitHookPreCommit,
				Glob:    glob.MustCompile("*.go"),
				Replace: "{}",
			},
			expectErr: nil,
		},
		{
			description: "invalid glob",
			configTask: configTask{
				Name: "some task",
				Exec: "foo bar baz",
				Glob: justStr("["),
			},
			expectRet: Task{},
			expectErr: fmt.Errorf(
				"Error compiling glob ([): %w",
				errors.New("unexpected end of input"),
			),
		},
		{
			description: "set replace",
			configTask: configTask{
				Name:    "some task",
				Exec:    "foo bar baz",
				Replace: justStr("___"),
			},
			expectRet: Task{
				Name:    "some task",
				Exec:    "foo bar baz",
				Hook:    GitHookPreCommit,
				Replace: "___",
			},
			expectErr: nil,
		},
		{
			description: "empty replace",
			configTask: configTask{
				Name:    "some task",
				Exec:    "foo bar baz",
				Replace: justStr(""),
			},
			expectRet: Task{},
			expectErr: fmt.Errorf(
				"%w: %v",
				ErrInvalidTaskReplace,
				configTask{
					Name:    "some task",
					Exec:    "foo bar baz",
					Replace: justStr(""),
				},
			),
		},
		{
			description: "only whitespace replace",
			configTask: configTask{
				Name: "some task",
				Exec: "foo bar baz",
				Replace: justStr("		 	  "),
			},
			expectRet: Task{},
			expectErr: fmt.Errorf(
				"%w: %v",
				ErrInvalidTaskReplace,
				configTask{
					Name: "some task",
					Exec: "foo bar baz",
					Replace: justStr("		 	  "),
				},
			),
		},
	}

	t.Parallel()
	for _, tc := range testCases {
		tc := tc // capture
		t.Run(tc.description, func(t *testing.T) {
			t.Parallel()

			ret, err := tc.configTask.ToTask()
			assert.Equal(t, tc.expectRet, ret)
			assert.Equal(t, tc.expectErr, err)
		})
	}
}

func TestConfigTaskString(t *testing.T) {
	justHook := func(h GitHook) *GitHook { return &h }

	testCases := []struct {
		description string
		configTask  configTask
		expectRet   string
	}{
		{
			description: "minimal",
			configTask: configTask{
				Exec: "foo bar baz",
				Name: "some task",
			},
			expectRet: `name="some task" exec="foo bar baz" hook=<nil> glob=<nil> dir=<nil>`,
		},
		{
			description: "different hook",
			configTask: configTask{
				Exec: "foo bar baz",
				Name: "some task",
				Hook: justHook(GitHookPostCommit),
			},
			expectRet: `name="some task" exec="foo bar baz" hook=post-commit glob=<nil> dir=<nil>`,
		},
	}

	t.Parallel()
	for _, tc := range testCases {
		tc := tc // capture
		t.Run(tc.description, func(t *testing.T) {
			t.Parallel()

			assert.Equal(t, tc.expectRet, tc.configTask.String())
		})
	}
}
