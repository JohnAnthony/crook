package crook

const HooksPath = ".crook"

type Crook struct {
	cfg *Config
}

func New(cfg *Config) (Crook, error) {
	return Crook{cfg: cfg}, nil
}
